package com.nucleo.apiavatar.rest;

import com.nucleo.apiavatar.ApiAvatarApplication;
import com.nucleo.apiavatar.config.TestingConfig;
import com.nucleo.apiavatar.dto.PersonaDTO;
import com.nucleo.apiavatar.mapper.PersonaMapper;
import com.nucleo.apiavatar.model.Persona;
import com.nucleo.apiavatar.repository.PersonaRepository;
import com.nucleo.apiavatar.service.PersonaService;
import com.nucleo.apiavatar.service.PlanetValidator;
import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;
import com.nucleo.apiavatar.service.query.PersonaQueryService;
import com.nucleo.apiavatar.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApiAvatarApplication.class, TestingConfig.class,})
public class PersonaResourceIntTest {

    private static final String DEFAULT_NAME = "Luke Skywalker";
    private static final String DEFAULT_HEIGHT = "172";
    private static final String DEFAULT_MASS = "77";
    private static final String DEFAULT_HAIR_COLOR = "blonde";
    private static final String DEFAULT_GENDER = "male";
    private static final String DEFAULT_PLANET = "Tatooine";

    private static final String UPDATE_NAME = "Helena";
    private static final String UPDATE_HEIGHT = "98";
    private static final String UPDATE_MASS = "21";
    private static final String UPDATE_HAIR_COLOR = "black";
    private static final String UPDATE_GENDER = "female";
    private static final String UPDATE_PLANET = "Alderaan";


    @Autowired
    private PersonaService personaService;
    @Autowired
    private PersonaQueryService personaQueryService;
    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private PersonaMapper mapper;

    @Autowired
    private PlanetValidator planetValidator;

    @Autowired
    private EntityManager entityManager;


    private MockMvc mockMvc;

    private Persona persona;

    public static Persona createEntity() {
        Persona persona = new Persona();
        persona.setName(DEFAULT_NAME);
        persona.setHeight(DEFAULT_HEIGHT);
        persona.setMass(DEFAULT_MASS);
        persona.setHairColor(DEFAULT_HAIR_COLOR);
        persona.setGender(DEFAULT_GENDER);
        persona.setPlanet(DEFAULT_PLANET);
        return persona;
    }

    @Before
    public void setUp() {
        PersonaResource resource = new PersonaResource(personaService, personaQueryService);
        this.mockMvc = MockMvcBuilders//
                .standaloneSetup(resource)//
                .setCustomArgumentResolvers(pageableArgumentResolver)//
                .build();
    }

    @Before
    public void initTest() {
        this.persona = createEntity();
    }

    @Test
    @Transactional
    public void getAllTest_HappyPath() throws Exception {
        //give
        this.personaRepository.saveAndFlush(persona);

        this.mockMvc.perform(get("/v1/persona"))
                .andExpect(status().isOk())//
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))//
                .andExpect(jsonPath("$.[*].id").value(hasItem(persona.getId())))//
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))//
                .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT)))//
                .andExpect(jsonPath("$.[*].mass").value(hasItem(DEFAULT_MASS)))//
                .andExpect(jsonPath("$.[*].hair_color").value(hasItem(DEFAULT_HAIR_COLOR)))//
                .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER)))//
                .andExpect(jsonPath("$.[*].planet").value(hasItem(DEFAULT_PLANET)))//
                .andExpect(header().exists(PersonaResource.HEADER_COUNT))
        ;
    }

    @Test
    @Transactional
    public void getAllTest_DataSetIsEmpty() throws Exception {
        this.mockMvc.perform(get("/v1/persona"))
                .andExpect(status().isOk())//
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.[*]").isEmpty())
        ;
    }

    @Test
    @Transactional
    public void getAllTest_SizeOnePageOne() throws Exception {
        this.personaRepository.saveAndFlush(createEntity());
        this.personaRepository.saveAndFlush(createEntity());
        this.mockMvc.perform(get("/v1/persona?size=1&page=0"))
                .andExpect(status().isOk())//
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.[*]").isNotEmpty())
                .andExpect(jsonPath("$.[*]").value(hasSize(1)))
        ;
    }

    @Test
    @Transactional
    public void getAllTest_SizeTwoPageTwo() throws Exception {
        int sizeDatabase = 50;
        List<Persona> lst = IntStream.range(0, sizeDatabase)//
                .mapToObj(value -> this.createEntity())//
                .collect(Collectors.toList());
        this.personaRepository.saveAll(lst);


        this.mockMvc.perform(get("/v1/persona?size=2&page=1"))
                .andExpect(status().isOk())//
                .andDo(print())//
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))//
                .andExpect(jsonPath("$.[*]").isNotEmpty())//
                .andExpect(jsonPath("$.[*]").value(hasSize(2)))//
        ;
    }

    @Test
    @Transactional
    public void createPersonaTest_HappyPath() throws Exception {
        int sizeDatabase = this.personaRepository.findAll().size();
        PersonaDTO body = mapper.toDto(persona);

        when(planetValidator.validatePlanet(any())).thenReturn(true);

        this.mockMvc.perform(post("/v1/persona")//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(header().exists("Location"))//
                .andExpect(status().isCreated())//
        ;

        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(sizeDatabase + 1);
        Persona testPersona = personaList.get(personaList.size() - 1);
        assertThat(testPersona.getMass()).isEqualTo(DEFAULT_MASS);
        assertThat(testPersona.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testPersona.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testPersona.getPlanet()).isEqualTo(DEFAULT_PLANET);
        assertThat(testPersona.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPersona.getHairColor()).isEqualTo(DEFAULT_HAIR_COLOR);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void createPersonaTest_BodyWithId() throws Exception {

        when(planetValidator.validatePlanet(any())).thenReturn(true);

        int sizeDatabaseBeforeUpdate = personaRepository.findAll().size();

        PersonaDTO body = mapper.toDto(persona);
        body.setId(1);
        this.mockMvc.perform(post("/v1/persona")//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isBadRequest())//
                .andExpect(status().reason(containsString("persona id must to be null")))
        ;
        List<Persona> lst = personaRepository.findAll();
        assertThat(lst).hasSize(sizeDatabaseBeforeUpdate);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void createPersonaTest_PlanetNotExists() throws Exception {
        int sizeDatabase = personaRepository.findAll().size();
        PersonaDTO body = mapper.toDto(persona);

        when(planetValidator.validatePlanet(anyString())).thenReturn(false);

        this.mockMvc.perform(post("/v1/persona")//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isInternalServerError())//
                .andExpect(status().reason(containsString("El nombre de planeta ingresado no existe")))
        ;

        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(sizeDatabase);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void createPersonaTest_ProviderError() throws Exception {
        int sizeDatabase = personaRepository.findAll().size();
        PersonaDTO body = mapper.toDto(persona);

        when(planetValidator.validatePlanet(anyString())).thenThrow(new ErrorPlanetValidatorProvider(""));

        this.mockMvc.perform(post("/v1/persona")//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isFailedDependency())//
        ;

        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(sizeDatabase);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void updatePersonaTest_ResourceNotExists() throws Exception {

        when(planetValidator.validatePlanet(any())).thenReturn(true);

        personaRepository.saveAndFlush(persona);
        int sizeDatabaseBeforeUpdate = personaRepository.findAll().size();

        int id = persona.getId();
        Persona updatePersona = personaRepository.findById(persona.getId()).get();
        entityManager.detach(updatePersona);
        PersonaDTO body = mapper.toDto(updatePersona);
        body.setId(null);
        body.setPlanet(UPDATE_PLANET);
        body.setMass(UPDATE_MASS);
        body.setGender(UPDATE_GENDER);
        body.setHairColor(UPDATE_HAIR_COLOR);
        body.setHeight(UPDATE_HEIGHT);
        body.setName(UPDATE_NAME);

        this.mockMvc.perform(put("/v1/persona/{id}", id + 1)//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isNotFound())//
        ;
        List<Persona> lst = personaRepository.findAll();
        assertThat(lst).hasSize(sizeDatabaseBeforeUpdate);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void updatePersonaTest_BodyWithId() throws Exception {

        when(planetValidator.validatePlanet(any())).thenReturn(true);

        personaRepository.saveAndFlush(persona);
        int sizeDatabaseBeforeUpdate = personaRepository.findAll().size();

        int id = persona.getId();
        Persona updatePersona = personaRepository.findById(persona.getId()).get();
        entityManager.detach(updatePersona);
        PersonaDTO body = mapper.toDto(updatePersona);
        body.setPlanet(UPDATE_PLANET);
        body.setMass(UPDATE_MASS);
        body.setGender(UPDATE_GENDER);
        body.setHairColor(UPDATE_HAIR_COLOR);
        body.setHeight(UPDATE_HEIGHT);
        body.setName(UPDATE_NAME);

        this.mockMvc.perform(put("/v1/persona/{id}", id)//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isBadRequest())//
                .andExpect(status().reason(containsString("persona id must to be null, it wont be updated")))
        ;
        List<Persona> lst = personaRepository.findAll();
        assertThat(lst).hasSize(sizeDatabaseBeforeUpdate);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void updatePersonaTest_Saved() throws Exception {

        when(planetValidator.validatePlanet(any())).thenReturn(true);

        personaRepository.saveAndFlush(persona);
        int sizeDatabaseBeforeUpdate = personaRepository.findAll().size();

        int id = persona.getId();
        Persona deatached = personaRepository.findById(persona.getId()).get();
        entityManager.detach(deatached);
        PersonaDTO body = mapper.toDto(deatached);
        body.setId(null);
        body.setName(UPDATE_NAME);
        body.setPlanet(UPDATE_PLANET);
        body.setMass(UPDATE_MASS);
        body.setGender(UPDATE_GENDER);
        body.setHairColor(UPDATE_HAIR_COLOR);
        body.setHeight(UPDATE_HEIGHT);


        this.mockMvc.perform(put("/v1/persona/{id}", id)//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isNoContent())//
        ;
        List<Persona> lstPersona = personaRepository.findAll();
        assertThat(lstPersona).as("The database size should be equal same to init").hasSize(sizeDatabaseBeforeUpdate);

        Persona testPersona = lstPersona.get(sizeDatabaseBeforeUpdate - 1);
        assertThat(testPersona.getMass()).as("Mass should be update").isEqualTo(UPDATE_MASS);
        assertThat(testPersona.getGender()).as("Gender should be update").isEqualTo(UPDATE_GENDER);
        assertThat(testPersona.getHeight()).as("Height should be update").isEqualTo(UPDATE_HEIGHT);
        assertThat(testPersona.getPlanet()).as("Planet should be update").isEqualTo(UPDATE_PLANET);
        assertThat(testPersona.getName()).as("Name should be update").isEqualTo(UPDATE_NAME);
        assertThat(testPersona.getHairColor()).as("Hair Color should be update").isEqualTo(UPDATE_HAIR_COLOR);

        reset(planetValidator);
    }


    @Test
    @Transactional
    public void updatePersonaTest_NamePlanetNotExists() throws Exception {

        when(planetValidator.validatePlanet(any())).thenReturn(false);

        personaRepository.saveAndFlush(persona);
        int sizeDatabaseBeforeUpdate = personaRepository.findAll().size();

        int id = persona.getId();
        Persona updatePersona = personaRepository.findById(persona.getId()).get();
        entityManager.detach(updatePersona);
        PersonaDTO body = mapper.toDto(updatePersona);
        body.setId(null);
        body.setPlanet(UPDATE_PLANET);
        body.setMass(UPDATE_MASS);
        body.setGender(UPDATE_GENDER);
        body.setHairColor(UPDATE_HAIR_COLOR);
        body.setHeight(UPDATE_HEIGHT);
        body.setName(UPDATE_NAME);

        this.mockMvc.perform(put("/v1/persona/{id}", id)//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isInternalServerError())//
                .andExpect(status().reason(containsString("El nombre de planeta ingresado no existe")))
        ;
        List<Persona> lst = personaRepository.findAll();
        assertThat(lst).hasSize(sizeDatabaseBeforeUpdate);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void updatePersonaTest_ProviderError() throws Exception {

        when(planetValidator.validatePlanet(anyString())).thenThrow(new ErrorPlanetValidatorProvider(""));

        personaRepository.saveAndFlush(persona);
        int sizeDatabaseBeforeUpdate = personaRepository.findAll().size();

        int id = persona.getId();
        Persona updatePersona = personaRepository.findById(persona.getId()).get();
        entityManager.detach(updatePersona);
        PersonaDTO body = mapper.toDto(updatePersona);
        body.setId(null);
        body.setPlanet(UPDATE_PLANET);
        body.setMass(UPDATE_MASS);
        body.setGender(UPDATE_GENDER);
        body.setHairColor(UPDATE_HAIR_COLOR);
        body.setHeight(UPDATE_HEIGHT);
        body.setName(UPDATE_NAME);

        this.mockMvc.perform(put("/v1/persona/{id}", id)//
                .contentType(TestUtil.APPLICATION_JSON_UTF8)//
                .content(TestUtil.convertObjectToJsonBytes(body)))//
                .andDo(print())//
                .andExpect(status().isFailedDependency())//
        ;
        List<Persona> lst = personaRepository.findAll();
        assertThat(lst).hasSize(sizeDatabaseBeforeUpdate);
        reset(planetValidator);
    }

    @Test
    @Transactional
    public void getByIdTest_NotFound() throws Exception {
        mockMvc.perform(get("/v1/persona/{id}", 1))//
                .andDo(print())//
                .andExpect(status().isNotFound())//
        ;
    }

    @Test
    @Transactional
    public void getByIdTest_Ok() throws Exception {
        personaRepository.saveAndFlush(persona);

        mockMvc.perform(get("/v1/persona/{id}", persona.getId()))//
                .andDo(print())//
                .andExpect(status().isOk())//
                .andExpect(jsonPath("id").value(is(persona.getId())))//
                .andExpect(jsonPath("name").value(is(DEFAULT_NAME)))//
                .andExpect(jsonPath("height").value(is(DEFAULT_HEIGHT)))//
                .andExpect(jsonPath("mass").value(is(DEFAULT_MASS)))//
                .andExpect(jsonPath("hair_color").value(is(DEFAULT_HAIR_COLOR)))//
                .andExpect(jsonPath("gender").value(is(DEFAULT_GENDER)))//
                .andExpect(jsonPath("planet").value(is(DEFAULT_PLANET)))//
        ;
    }

    @Test
    public void detelePersona_NotFound() throws Exception {
        mockMvc.perform(delete("/v1/persona/{id}", 1))//
                .andDo(print())//
                .andExpect(status().isNotFound())//
        ;
    }

    @Test
    @Transactional
    public void detelePersona_Ok() throws Exception {
        personaRepository.saveAndFlush(persona);
        int sizeDatabase = personaRepository.findAll().size();
        mockMvc.perform(delete("/v1/persona/{id}", persona.getId()))//
                .andDo(print())//
                .andExpect(status().isOk())//
        ;
        List<Persona> personaList = personaRepository.findAll();
        assertThat(personaList).hasSize(sizeDatabase - 1);
    }


}
package com.nucleo.apiavatar.swapi;

import com.nucleo.apiavatar.config.TestingConfig;
import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SwapiPlanetValidator.class, TestingConfig.class})
public class SwapiPlanetValidatorTest {

    @Autowired
    private SwapiPlanetValidator planetValidator;

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void validateTest_HappyPath() throws ErrorPlanetValidatorProvider {
        Planet planet = new Planet();
        planet.setName("t");
        ResponsePlanet body = new ResponsePlanet();
        body.setPlanets(Arrays.asList(planet));
        ResponseEntity<ResponsePlanet> responseEntity = new ResponseEntity<>(body, HttpStatus.OK);
        given(restTemplate.getForEntity(anyString(), ArgumentMatchers.<Class<ResponsePlanet>>any())).willReturn(responseEntity);
        boolean valid = this.planetValidator.validatePlanet("t");
        assertThat(valid).isTrue();
    }

    @Test(expected = ErrorPlanetValidatorProvider.class)
    public void validateTest_StatusNot2XX() throws ErrorPlanetValidatorProvider {
        ResponseEntity<ResponsePlanet> responseEntity = new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
        given(restTemplate.getForEntity(anyString(), ArgumentMatchers.<Class<ResponsePlanet>>any())).willReturn(responseEntity);
        this.planetValidator.validatePlanet("t");
    }

    @Test
    public void validateTest_NotExistsName() throws ErrorPlanetValidatorProvider {
        ResponsePlanet body = new ResponsePlanet();
        body.setPlanets(Arrays.asList());
        ResponseEntity<ResponsePlanet> responseEntity = new ResponseEntity<>(body, HttpStatus.OK);
        given(restTemplate.getForEntity(anyString(), ArgumentMatchers.<Class<ResponsePlanet>>any())).willReturn(responseEntity);
        boolean valid = this.planetValidator.validatePlanet("t");
        assertThat(valid).isFalse();
    }
}
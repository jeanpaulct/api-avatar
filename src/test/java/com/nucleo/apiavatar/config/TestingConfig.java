package com.nucleo.apiavatar.config;

import com.nucleo.apiavatar.service.PlanetValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.mock;

@Configuration
public class TestingConfig {

    @Bean
    public RestTemplate restTemplate() {
        return mock(RestTemplate.class);
    }

    @Bean
    public PlanetValidator planetValidator() {
        return mock(PlanetValidator.class);
    }
}

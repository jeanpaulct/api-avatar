CREATE TABLE persona
(
  id         serial,
  gender     varchar(255),
  hair_color varchar(255),
  height     varchar(255),
  mass       varchar(255),
  name       varchar(255),
  planet     varchar(255),
  primary key (id)
);
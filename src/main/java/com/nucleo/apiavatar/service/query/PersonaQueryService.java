package com.nucleo.apiavatar.service.query;

import com.nucleo.apiavatar.dto.PersonaDTO;
import com.nucleo.apiavatar.mapper.PersonaMapper;
import com.nucleo.apiavatar.repository.PersonaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PersonaQueryService {

    private final static Logger log = LoggerFactory.getLogger(PersonaQueryService.class);

    private final PersonaRepository repository;

    private final PersonaMapper mapper;

    public PersonaQueryService(PersonaRepository repository, PersonaMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public Page<PersonaDTO> getAll(Pageable pageable) {
        log.debug("Find personas");
        return this.repository.findAll(pageable)
                .map(mapper::toDto);
    }
}

package com.nucleo.apiavatar.service.impl;

import com.nucleo.apiavatar.dto.PersonaDTO;
import com.nucleo.apiavatar.mapper.PersonaMapper;
import com.nucleo.apiavatar.model.Persona;
import com.nucleo.apiavatar.repository.PersonaRepository;
import com.nucleo.apiavatar.service.PersonaService;
import com.nucleo.apiavatar.service.PlanetValidator;
import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;
import com.nucleo.apiavatar.service.errors.NamePlanetNotExistsException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PersonaServiceImpl implements PersonaService {

    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;
    private final PlanetValidator planetValidator;

    public PersonaServiceImpl(PersonaRepository personaRepository, PersonaMapper personaMapper, PlanetValidator planetValidator) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
        this.planetValidator = planetValidator;
    }

    @Override
    public PersonaDTO create(PersonaDTO personaDTO) throws NamePlanetNotExistsException, ErrorPlanetValidatorProvider {
        return this.savePersona(personaDTO);
    }

    @Override
    public Optional<PersonaDTO> findById(Integer id) {
        return personaRepository.findById(id).map(personaMapper::toDto);
    }

    @Override
    public PersonaDTO update(PersonaDTO personaDTO) throws NamePlanetNotExistsException, ErrorPlanetValidatorProvider {
        return this.savePersona(personaDTO);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        personaRepository.deleteById(id);
    }

    protected PersonaDTO savePersona(PersonaDTO personaDTO) throws ErrorPlanetValidatorProvider, NamePlanetNotExistsException {
        if (!planetValidator.validatePlanet(personaDTO.getPlanet()))
            throw new NamePlanetNotExistsException("El nombre de planeta ingresado no existe");
        return this.persistPersona(personaDTO);
    }

    @Transactional
    protected PersonaDTO persistPersona(PersonaDTO personaDTO) {
        Persona persona = personaMapper.toEntity(personaDTO);
        persona = personaRepository.save(persona);
        return personaMapper.toDto(persona);
    }
}

package com.nucleo.apiavatar.service;

import com.nucleo.apiavatar.dto.PersonaDTO;
import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;
import com.nucleo.apiavatar.service.errors.NamePlanetNotExistsException;

import java.util.Optional;

public interface PersonaService {
    PersonaDTO create(PersonaDTO personaDTO) throws NamePlanetNotExistsException, ErrorPlanetValidatorProvider;

    Optional<PersonaDTO> findById(Integer id);

    PersonaDTO update(PersonaDTO personaDTO) throws NamePlanetNotExistsException, ErrorPlanetValidatorProvider;

    void deleteById(Integer id);
}

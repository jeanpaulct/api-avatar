package com.nucleo.apiavatar.service.errors;

public class ErrorPlanetValidatorProvider extends Exception {
    public ErrorPlanetValidatorProvider(String s) {
        super(s);
    }
}

package com.nucleo.apiavatar.service.errors;

public class NamePlanetNotExistsException extends Exception {
    public NamePlanetNotExistsException(String s) {
        super(s);
    }
}

package com.nucleo.apiavatar.service;

import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;

public interface PlanetValidator {
    boolean validatePlanet(String name) throws ErrorPlanetValidatorProvider;
}

package com.nucleo.apiavatar.rest.config;


import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequest;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class MyErrorAttribute extends DefaultErrorAttributes {
    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Throwable throwable = getError(webRequest);
        if (throwable != null) {
            Map<String, Object> errorAttributes = new LinkedHashMap<>();
            Throwable cause = throwable.getCause();
            if (cause != null) {
                errorAttributes.put("error_message", cause.getMessage());
                return errorAttributes;
            }
        }
        return super.getErrorAttributes(webRequest, includeStackTrace);
    }
}
package com.nucleo.apiavatar.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration {

    @Bean
    public WebMvcConfigurer webMvcConfigurer(CorsProperties corsProps) {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                CorsRegistration corsRegistration = registry.addMapping("/**");
                if (StringUtils.hasText(corsProps.getAllowedOrigins()))
                    corsRegistration.allowedOrigins(corsProps.getAllowedOrigins());
                if (StringUtils.hasText(corsProps.getAllowedHeaders()))
                    corsRegistration.allowedHeaders(corsProps.getAllowedHeaders());
                if (StringUtils.hasText(corsProps.getAllowedMethods()))
                    corsRegistration.allowedMethods(corsProps.getAllowedMethods());
                if (StringUtils.hasText(corsProps.getExposedHeaders()))
                    corsRegistration.exposedHeaders(corsProps.getExposedHeaders());
            }
        };
    }
}

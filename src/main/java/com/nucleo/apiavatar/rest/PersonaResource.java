package com.nucleo.apiavatar.rest;

import com.nucleo.apiavatar.dto.PersonaDTO;
import com.nucleo.apiavatar.service.PersonaService;
import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;
import com.nucleo.apiavatar.service.errors.NamePlanetNotExistsException;
import com.nucleo.apiavatar.service.query.PersonaQueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/persona")
public class PersonaResource {

    protected static final String HEADER_COUNT = "X-Total-Count";
    private final static Logger log = LoggerFactory.getLogger(PersonaResource.class);
    private final PersonaService personaService;
    private final PersonaQueryService personaQueryService;


    public PersonaResource(PersonaService service, PersonaQueryService personaQueryService) {
        this.personaService = service;
        this.personaQueryService = personaQueryService;
    }

    @GetMapping
    public ResponseEntity<List<PersonaDTO>> getAll(Pageable pageable) {
        log.debug("REST request to get Personas");
        Page<PersonaDTO> result = this.personaQueryService.getAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HEADER_COUNT, Long.toString(result.getTotalElements()));
        return ResponseEntity.ok().headers(headers).body(result.getContent());
    }

    @PostMapping
    public ResponseEntity createPerson(@Valid @RequestBody PersonaDTO personaDTO)
            throws URISyntaxException {
        log.debug("REST request to create Persona {}", personaDTO);
        if (personaDTO.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "persona id must to be null", new Exception("persona id must to be null"));
        }
        PersonaDTO result;
        try {
            result = personaService.create(personaDTO);
        } catch (NamePlanetNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        } catch (ErrorPlanetValidatorProvider e) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, e.getMessage(), e);
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity updatePerson(@PathVariable("id") Integer id, @Valid @RequestBody PersonaDTO personaDTO) {
        log.debug("REST request to update Persona {} with id {}", personaDTO, id);
        if (personaDTO.getId() != null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "persona id must to be null, it wont be updated", new Exception("persona id must to be null, it wont be updated"));
        Optional<PersonaDTO> op = personaService.findById(id);
        if (!op.isPresent())
            return ResponseEntity.notFound().build();
        personaDTO.setId(id);
        try {
            this.personaService.update(personaDTO);
        } catch (NamePlanetNotExistsException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        } catch (ErrorPlanetValidatorProvider e) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, e.getMessage(), e);
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonaDTO> getPerson(@PathVariable("id") Integer id) {
        log.debug("REST request to get Persona with id {}", id);
        Optional<PersonaDTO> opt = personaService.findById(id);
        if (!opt.isPresent())
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(opt.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePerson(@PathVariable("id") Integer id) {
        log.debug("REST request to delete Persona with id {}", id);
        Optional<PersonaDTO> opt = personaService.findById(id);
        if (!opt.isPresent())
            return ResponseEntity.notFound().build();
        personaService.deleteById(id);
        return ResponseEntity.ok().build();
    }


}

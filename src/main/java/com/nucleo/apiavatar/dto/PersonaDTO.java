package com.nucleo.apiavatar.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.StringJoiner;

public class PersonaDTO {

    private Integer id;
    private String name;
    private String height;
    private String mass;
    private String gender;
    @NotNull
    @Size(min = 1)
    private String planet;
    @JsonProperty("hair_color")
    private String hairColor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPlanet() {
        return planet;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PersonaDTO.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("height='" + height + "'")
                .add("mass='" + mass + "'")
                .add("gender='" + gender + "'")
                .add("planet='" + planet + "'")
                .add("hairColor='" + hairColor + "'")
                .toString();
    }
}

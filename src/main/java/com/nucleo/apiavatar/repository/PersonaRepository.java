package com.nucleo.apiavatar.repository;

import com.nucleo.apiavatar.model.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {
}

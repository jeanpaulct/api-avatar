package com.nucleo.apiavatar.swapi;

import com.nucleo.apiavatar.service.PlanetValidator;
import com.nucleo.apiavatar.service.errors.ErrorPlanetValidatorProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Component
public class SwapiPlanetValidator implements PlanetValidator {

    private static final Logger log = LoggerFactory.getLogger(SwapiPlanetValidator.class);
    private final RestTemplate restTemplate;
    @Value("${com.nucleo.avatar.validate.planet.url}")
    private String endpoint;
    @Value("${com.nucleo.avatar.validate.planet-url:false}")
    private boolean ignoreCase;

    public SwapiPlanetValidator(RestTemplate swapiClient) {
        this.restTemplate = swapiClient;
    }

    @Override
    public boolean validatePlanet(String param) throws ErrorPlanetValidatorProvider {
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromUriString(endpoint)
                .queryParam("search", param);
        return this.validate(urlBuilder.build(false).toString(), param);
    }

    protected boolean validate(String url, String param) throws ErrorPlanetValidatorProvider {
        log.debug("URL to validate: {}", url);
        ResponseEntity<ResponsePlanet> response = this.restTemplate.getForEntity(url, ResponsePlanet.class);
        if (!response.getStatusCode().is2xxSuccessful()) {
            log.error("Error trying perform validation of Planet [{}] Code: [{}]", param, response.getStatusCode());
            throw new ErrorPlanetValidatorProvider("The validator provider has errors, try again.");
        }

        ResponsePlanet body = response.getBody();

        if (body.getPlanets().isEmpty())
            return false;

        Optional<Planet> planet = body.getPlanets()
                .stream()
                .filter(p -> ignoreCase ? param.equalsIgnoreCase(p.getName()) : param.equals(p.getName()))
                .findFirst();

        if (planet.isPresent())
            return true;
        if (!StringUtils.hasText(body.getNext()))
            return false;
        return validate(body.getNext(), param);
    }
}

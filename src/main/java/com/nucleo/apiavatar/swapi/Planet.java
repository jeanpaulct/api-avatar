package com.nucleo.apiavatar.swapi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Planet {

    @JsonProperty("name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

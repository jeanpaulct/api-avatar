package com.nucleo.apiavatar.swapi;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SwapiClientConfiguration {


    @Bean("swapiClient")
    public RestTemplate restTemplate(RestTemplateBuilder builder, @Value("${com.nucleo.avatar.validate.planet.timeout:5}") int timeout) {
        RequestConfig config = RequestConfig.custom()//
                .setConnectTimeout(timeout * 1000)//
                .setConnectionRequestTimeout(timeout * 1000)//
                .setSocketTimeout(timeout * 1000)//
                .build();
        CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(config)
                .setSSLHostnameVerifier((str, ssl) -> true) //TODO sslverify, we don't have the certified download and setting it
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return builder.requestFactory(() -> requestFactory).build();
    }
}

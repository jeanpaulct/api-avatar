package com.nucleo.apiavatar.swapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponsePlanet {

    private Integer count;
    private String previous;
    private String next;

    @JsonProperty("results")
    private List<Planet> planets;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }
}

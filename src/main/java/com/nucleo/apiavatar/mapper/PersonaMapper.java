package com.nucleo.apiavatar.mapper;

import com.nucleo.apiavatar.dto.PersonaDTO;
import com.nucleo.apiavatar.model.Persona;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface PersonaMapper extends EntityMapper<PersonaDTO, Persona> {

}

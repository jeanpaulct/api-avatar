package com.nucleo.apiavatar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class ApiAvatarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiAvatarApplication.class, args);
    }

}
